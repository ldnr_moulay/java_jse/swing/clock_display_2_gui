import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JSeparator;
import javax.swing.JMenuItem;
import java.awt.FlowLayout;

/**
 * D�crivez votre classe Gui ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class Gui extends JFrame
{
private JPanel containerBoutons;
private JPanel containerAffiche;
private JPanel containerHead;
private JButton boutonStart;
private JButton boutonStop;
private boolean clockIsRunning = false;
private TimerThread timerThread;

JLabel label = new JLabel("00 : 00");

//instance de clockDisplay
ClockDisplay clock = new ClockDisplay();

//Constructor
public Gui(){
//Fenetre
this.setSize(300, 150);
this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Fermer le programme avec la fen�tre
this.setLocationRelativeTo(null); // Centrer la fen�tre sur l��cran
this.setVisible(true);
this.setLayout(new BorderLayout());

JMenuBar menuBar = new JMenuBar();


JMenu menu = new JMenu("File");

JMenu submenu = new JMenu("About");
menu.add(new JSeparator());
submenu.add(new JMenuItem("TODO"));
menu.add(submenu);

//Panels
containerBoutons=new JPanel();
containerAffiche=new JPanel();
//containerHead=new JPanel();


//Attribution aux variables boutons l'instanciation des objets JButton
boutonStart = new JButton("Start");
boutonStart.setPreferredSize(new Dimension(120, 50));
boutonStop = new JButton("Stop");
boutonStop.setPreferredSize(new Dimension(120, 50));

//Ajout des boutons au containerBoutons
containerBoutons.add(boutonStart);
containerBoutons.add(boutonStop);

//Ajout du label au containerAffiche
containerAffiche.add(label);

//Ajout de file � containerHead
//containerHead.add(menu);

//Ajout du container a la fenetre
this.add(containerBoutons, BorderLayout.SOUTH);
this.add(containerAffiche, BorderLayout.NORTH);
//this.add(containerHead, BorderLayout.LINE_START);


//Ajout des listeners
boutonStart.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent e) {
        timerThread = new TimerThread();
        timerThread.start();
    }
});

boutonStop.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
        clockIsRunning = false;
        boutonStart.setEnabled(true);
        
    }
});

}

public void avance(){
    clockIsRunning = true;
    while(clockIsRunning){
        clock.timeTick();
        label.setText(""+clock.getMyDisplayString());
        timerThread.pause();
        boutonStart.setEnabled(false);
}
}

class TimerThread extends Thread
{
        public void run()
        {
            avance();
        }
        
        private void pause()
        {
            try {
                Thread.sleep(1000);   // pause for 300 milliseconds
            }
            catch (InterruptedException exc) {
                System.out.println("hello");
            }
        }
}
}
