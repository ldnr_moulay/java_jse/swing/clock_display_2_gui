/**
 * Simule l'affichage d'une montre digitale
 * L'affichage est simul� par la valeur de la propri�t� "myDisplayString"
 */
public class ClockDisplay
{
    private String myDisplayString;

    // m�thode qui retourne myDisplayString
    private NumberDisplay heure;
    private NumberDisplay minute;

    /**
     * Constructor for objects of class ClockDisplay
     * Initialisation avec des heures / minutes par d�faut
     */
    public ClockDisplay()
    {
        heure = new NumberDisplay(23, 24);
        minute = new NumberDisplay(58, 60);
        
        
        //raffraichirDisplay();
    }

    /**
     * Initialise un ClockDisplay avec les valeurs sp�cifi�es
     * @param hours : la valeur des heures 
     * @param minutes : la valeur des minutes 
     */
    public ClockDisplay(int h, int m) {
        heure = new NumberDisplay(h, 24);
        minute = new NumberDisplay(m, 60);
        raffraichirDisplay();
    }

    private void raffraichirDisplay()
    {
        myDisplayString = heure.getDisplayValue() + ":" + minute.getDisplayValue();
    }

    /**
     *  Simule le passage d'une minute
     */
    public void timeTick()
    {
        minute.increment();
        if (minute.getValue() == 0)
            heure.increment();
        raffraichirDisplay();
    }

    /**
     * Getter de la propri�t� myDisplayString
     */
    public String getMyDisplayString()
    {
        return myDisplayString;
    }
}
