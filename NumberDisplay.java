
/**
 * D�crivez votre classe NumberDisplay ici.
 *
 * @author (votre nom)
 * @version (un num�ro de version ou une date)
 */
public class NumberDisplay
{
    private int value;
    private int limit;

    /**
     * Constructeur d'objets de classe NumberDisplay
     */
    public NumberDisplay(int value, int limit)
    {

        if (value < 0 || value >= limit)
            value = 0;
        this.value = value;
        this.limit = limit;
    }
    
    public int getValue()
    {
        return value;
    }

    /**
     * Augmenter le nombre, passe � 0 si limit atteint
     * test: 12,24->13    23,24->0
     */
    public void increment()
    {
        
        value += 1;
        if (value >= limit)
            value = 0;
        

        // value = (value+1) % limit;
    }

    /**
     * Retourne le nombre formatt� (pr�fix� de 0 au besoin)
     * Retourne le NumberDisplay sous forme de cha�ne 0-pr�fix�e 
     */
    public String getDisplayValue()
    {
        if (value < 10)
            return "0" + value;
        else
            return "" + value;        
    }
    
    private String formatNombre(int nb)
    {
    // return (nb < 10) ? "0" + nb : "0" + nb;
    
    if (nb < 10)
        return "0" + nb;
    else
        return "" + nb;
    }
}
